# debian-build

Docker image for Debian package builds

## Available images

[Debian](https://hub.docker.com/repository/docker/beagle/debian-build/general)

* `beagle/debian-build:latest`: amd64, armhf, arm64 - Bookworm
* `beagle/debian-build:bookworm`: amd64, armhf, arm64 - Bookworm
* `beagle/debian-build:bullsye`: amd64, armhf, arm64 - Bullseye

[Ubuntu](https://hub.docker.com/repository/docker/beagle/ubuntu-build/general)

* `beagle/ubuntu-build:latest`: amd64, armhf, arm64, riscv64 - 23.10
