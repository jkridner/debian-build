ARG FRM=jkridner/ubuntu-24-04-$TARGETARCH:latest
ARG TARGETARCH

FROM ${FRM} as build
ENV DEBIAN_FRONTEND=noninteractive
ENV BALENA_CLI_URL=https://dl.beagle.cc/balena-cli-v18.2.34-linux-x64-standalone.zip

RUN apt-get update \
    && apt-get install -y apt-utils ca-certificates \
    && apt-get clean

#RUN apt-get update \
#    && apt-get install -y locales \
#    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8 \
#    && apt-get clean
#ENV LANG en_US.utf8

RUN . /etc/os-release \
    && export ARCHNAME="$(dpkg --print-architecture)" \
    && echo "Setting release to ${VERSION_CODENAME}..." \
    && echo "Setting arch to ${ARCHNAME}..." \
    && echo "Setting distro to ${ID}..." \
    && echo "deb [trusted=yes] https://debian.beagle.cc/${ARCHNAME}/ ${VERSION_CODENAME} main" > /etc/apt/sources.list.d/beagle.list \
    && echo "deb-src [trusted=yes] https://debian.beagle.cc/${ARCHNAME}/ ${VERSION_CODENAME} main" >> /etc/apt/sources.list.d/beagle.list \
    && apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends \
        docker.io    \
        bbb.io-keyring    \
        apindex    \
        cmake    \
        build-essential \
        debhelper-compat \
        devscripts \
        curl gpg git wget tar \
        autoconf libtool autotools-dev \
        libssl-dev libxml2-dev libyaml-dev libgmp-dev libz-dev \
        file cpio unzip bc rsync bison flex device-tree-compiler python3-setuptools swig python3-dev ccache \
    && cp -v /etc/bbb.io/templates/apt/sbuild.list /etc/apt/sources.list.d/beagle.list \
    && apt-get clean
RUN export ARCHNAME="$(dpkg --print-architecture)" ; \
    if [ "${ARCHNAME}" = "amd64" ] ; then \
        wget "${BALENA_CLI_URL}" -O x.zip ; \
        mkdir -p /opt ; \
        unzip x.zip -d /opt ; \
        rm x.zip ; \
        echo "export PATH=\$PATH:/opt/balena-cli" >> /etc/environment ; \
    fi